package library;

public class TestLibrary {
    public static void main(String[] args) {
        // Создаем пять книг
        Book book1 = new Book("Java1", "Эккель");
        Book book2 = new Book("Java2", "Эккель");
        Book book3 = new Book("Java3", "Эккель");
        Book book4 = new Book("Java4", "Эккель");
        Book book5 = new Book("Java5", "Эккель");

        System.out.println("Создаем библиотеку на 4 книги");
        Library library = new Library(4);
        System.out.println(library);

        System.out.println("Добавили книгу 1");
        library.addBook(book1);
        System.out.println(library);

        System.out.println("Добавили книгу 2");
        library.addBook(book2);
        System.out.println(library);

        System.out.println("Добавили книгу 3");
        library.addBook(book3);
        System.out.println(library);

        System.out.println("Ищем книгу 2");
        System.out.println("Номер в списке book2: " + library.searchBook(book2) + "\n");

        System.out.println("Удаляем книгу 2");
        library.removeBook(book2);
        System.out.println(library);

        System.out.println("Ищем книгу 2");
        System.out.println("Номер в списке book2: " + library.searchBook(book2) + "\n");

        System.out.println("Добавили книгу 2");
        library.addBook(book2);
        System.out.println(library);

        System.out.println("Добавляем книгу 4");
        library.addBook(book4);
        System.out.println(library);

        System.out.println("Добавляем книгу 5, наблюдаем что добавление игнорируется");
        library.addBook(book5);
        System.out.println(library);

        System.out.println("Удаляем книгу 2");
        library.removeBook(book2);
        System.out.println(library);

        System.out.println("Удаляем книгу 4");
        library.removeBook(book4);
        System.out.println(library);
    }
}
