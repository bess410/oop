package library;

public class Library {
    private Book[] listBook;
    private static int counter = 0;

    public Library(int size) {
        listBook = new Book[size];
    }


    public void setListBook(Book[] listBook) {
        this.listBook = listBook;
    }

    public void addBook(Book book) {
        // Если книги нет в списке книг библиотеки
        if (searchBook(book) == -1) {
            // Если в списке нет места, то игнорируем добавление
            if (counter == listBook.length) {
                return;
            }
            listBook[counter++] = book;
        }
    }

    public int searchBook(Book book) {
        for (int i = 0; i < counter; i++) {
            if (listBook[i].equals(book)) {
                return i;
            }
        }
        return -1;
    }

    public void removeBook(Book book) {
        // Ищем позицию книги в списке
        int position = searchBook(book);
        if (position != -1) {
            // Если наша книга не на последней позиции
            // сдвигаем все книги влево
            // и уменьшаем счетчик
            if (position != counter - 1) {
                System.arraycopy(listBook, position + 1, listBook, position, counter - position - 1);
            }
            listBook[--counter] = null;
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Library:\n");
        for (Book aListBook : listBook) {
            sb.append("\t").append(aListBook).append("\n");
        }
        return sb.toString();
    }
}
