package library;

import java.util.Objects;

public class Book {
    private String title;
    private String author;

    @Override
    public String toString() {
        return "Book{" +
                "title='" + title + '\'' +
                ", author='" + author + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Book other = (Book) obj;
        return Objects.equals(this.author, other.author) && Objects.equals(this.title, other.title);
    }

    public Book(String title, String author) {
        this.title = title;
        this.author = author;
    }
}
