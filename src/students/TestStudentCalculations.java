package students;

import students.services.GroupProgressService;
import students.services.MarkCalculationService;
import students.students.Student;
import students.testing.StudentGenerator;
import students.students.Group;

public class TestStudentCalculations {
    public static void main(String[] args) {
        // Создаем генератор случайных студентов
        StudentGenerator generator = new StudentGenerator();
        // Создаем калькулятор для расчетов оценок
        MarkCalculationService calculator = new MarkCalculationService();
        // Создаем калькулятор группы для выявления отличников и двоечников
        GroupProgressService groupCalculator = new GroupProgressService();
        // Генерируем группу студентов с количеством студентов, количеством оценок
        // и минимально возможной оценкой в зачетках
        Group group = new Group("Java group", generator.getRandomStudents(10, 5, 2));

        System.out.printf("The middle mark of the group is %4.2f\n", calculator.getGroupMiddleMark(group));
        for (Student student : group.getListStudent()) {
            System.out.printf("Name: %s, The middle mark: %4.2f\n", student.getName(), calculator.getStudentMiddleMark(student));
        }
        System.out.println("The number of students having two is " + groupCalculator.getNumberHaveTwo(group));
        System.out.println("The number of excellent students is " + groupCalculator.getNumberHaveExcellent(group));
    }
}
