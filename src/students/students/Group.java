package students.students;

public class Group {
    private String name;
    private Student[] listStudent;

    public int getSize() {
        return listStudent.length;
    }

    public String getName() {
        return name;
    }

    public Group(String name, Student[] listStudent) {
        this.name = name;

        this.listStudent = listStudent;
    }

    public Student[] getListStudent() {
        return listStudent;
    }
}
