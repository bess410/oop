package students.students;

public class Student {
    private String name;
    private StudentProgress progress;

    public String getName() {
        return name;
    }

    public StudentProgress getProgress() {
        return progress;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", progress=" + progress +
                '}';
    }

    public Student(String name, StudentProgress progress) {
        this.name = name;
        this.progress = progress;
    }
}
