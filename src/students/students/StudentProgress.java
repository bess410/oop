package students.students;

import java.util.Arrays;

public class StudentProgress {
    private int[] marks;

    public StudentProgress(int[] marks) {
        for (int mark : marks) {
            if (mark < 2 || mark > 5) {
                throw new IllegalStateException("The mark is out range " + mark);
            }
        }
        this.marks = marks;
    }

    public int[] getMarks() {
        return marks;
    }

    @Override
    public String toString() {
        return "StudentProgress{" +
                "marks=" + Arrays.toString(marks) +
                '}';
    }
}
