package students.testing;

import students.students.Student;
import students.students.StudentProgress;

public class StudentGenerator {
    // Получить случайные оценки для студента
    public StudentProgress getRandomStudentProgress(int size, int minMark) {
        int[] marks = new int[size];
        for (int i = 0; i < size; i++) {
            marks[i] = (int) (Math.random() * (6 - minMark)) + minMark;
        }
        return new StudentProgress(marks);
    }

    // Получить случайный список студентов
    public Student[] getRandomStudents(int numberStudents, int numberMarks, int minMark) {
        Student[] students = new Student[numberStudents];
        for (int i = 0; i < students.length; i++) {
            students[i] = new Student("student " + (i + 1), getRandomStudentProgress(numberMarks, minMark));
        }
        return students;
    }
}
