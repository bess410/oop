package students.services;

import students.students.Group;
import students.students.Student;

public class GroupProgressService {
    public int getNumberHaveExcellent(Group group) {
        MarkCalculationService markService = new MarkCalculationService();
        int count = 0;
        for (Student student : group.getListStudent()) {
            int[] marks = student.getProgress().getMarks();
            // Если есть двойки или тройки, то таких отличников нам не нужно
            if (haveMark(2, marks) || haveMark(3, marks)) {
                continue;
            }
            // Пятерок должно быть больше 75%
            if (Double.compare(markService.getStudentMiddleMark(student), 4.75) == 1) {
                count++;
            }
        }
        return count;
    }

    public int getNumberHaveTwo(Group group) {
        int count = 0;
        for (Student student : group.getListStudent()) {
            int[] marks = student.getProgress().getMarks();
            if (haveMark(2, marks)) {
                count++;
            }
        }
        return count;
    }

    // Поиск значения в массиве
    private boolean haveMark(int needMark, int[] marks) {
        for (int mark : marks) {
            if (mark == needMark) {
                return true;
            }
        }
        return false;
    }
}
