package students.services;

import students.students.Group;
import students.students.Student;

public class MarkCalculationService {
    public double getStudentMiddleMark(Student student) {
        int[] marks = student.getProgress().getMarks();
        double sum = 0;
        for (int mark : marks) {
            sum += mark;
        }
        return sum / marks.length;
    }

    public double getGroupMiddleMark(Group group) {
        Student[] students = group.getListStudent();
        double sum = 0;
        for (Student student : students) {
            sum += getStudentMiddleMark(student);
        }
        return sum / group.getSize();
    }
}
