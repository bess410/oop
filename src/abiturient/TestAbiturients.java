package abiturient;

import abiturient.abiturient.Abiturient;
import abiturient.services.AbiturientCalculationService;
import abiturient.services.AbiturientGenerator;
import abiturient.services.PrintService;

public class TestAbiturients {
    public static void main(String[] args) {
        // Создаем генератор для получения тестовых данных
        AbiturientGenerator generator = new AbiturientGenerator();
        Abiturient[] list = generator.getRandomAbiturientList(10, 5, 2);
        // Создаем службу для вывода на консолль
        PrintService printService = new PrintService();
        printService.print("Список всех абитуриентов и их общий балл:");
        printService.printAbiturientList(list, list.length);
        // Создаем службу для выполнения расчетов над абитуриентами
        AbiturientCalculationService acs = new AbiturientCalculationService();
        printService.print();
        printService.print("Список тех, кто прошел:");
        // Печатаем необходимое число абитуриентов
        printService.printAbiturientList(acs.getSortAbiturientsByTotal(list), 5);
    }
}
