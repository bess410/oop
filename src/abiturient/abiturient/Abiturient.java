package abiturient.abiturient;

public class Abiturient {
    private String name;
    private int[] examMarks;

    public String getName() {
        return name;
    }

    public int[] getExamMarks() {
        return examMarks;
    }

    public Abiturient(String name, int[] examMarks) {
        this.name = name;
        this.examMarks = examMarks;

    }

    @Override
    public String toString() {
        return name;
    }
}
