package abiturient.services;

import abiturient.abiturient.Abiturient;

public class AbiturientCalculationService {
    // Получение общего количества баллов за экзамены
    public int getTotal(Abiturient abiturient) {
        int total = 0;
        for (int mark : abiturient.getExamMarks()) {
            total += mark;
        }
        return total;
    }

    // Сортировка студентов без использования Map, по общему баллу
    public Abiturient[] getSortAbiturientsByTotal(Abiturient[] abiturients) {
        Abiturient[] list = new Abiturient[abiturients.length];

        for (int i = 0; i < abiturients.length; i++) {
            for (int j = 0; j < i; j++) {
                // Для каждого следующего абитуриента:
                // 1) сравниваем общий балл с общим баллом тех, кто уже в списке
                // 2) если общий балл больше общего балла кого-то из списка,
                //    то сдвигаем весь список с этой позиции вправо и на его
                //    место вставляем нашего абитуриента
                // 3) Переходим к следующему абитуриенту
                if (getTotal(abiturients[i]) > getTotal(list[j])) {
                    System.arraycopy(list, j, list, j + 1, i - j);
                    list[j] = abiturients[i];
                    break;
                }
            }
            // Если общий балл меньше всех, вставляем абитуриента в конец списка
            if (list[i] == null) {
                list[i] = abiturients[i];
            }
        }

        return list;
    }
}
