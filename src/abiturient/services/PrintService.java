package abiturient.services;

import abiturient.abiturient.Abiturient;

public class PrintService {
    // Напечатать определенное количество абитуриентов из списка
    public void printAbiturientList(Abiturient[] abiturients, int numberRecords) {
        AbiturientCalculationService acs = new AbiturientCalculationService();
        int length = abiturients.length >= numberRecords ? numberRecords : abiturients.length;
        for (int i = 0; i < length; i++) {
            System.out.printf("%10s : %d\n", abiturients[i], acs.getTotal(abiturients[i]));
        }
    }

    public void print(String string) {
        System.out.println(string);
    }

    public void print() {
        System.out.println();
    }
}
