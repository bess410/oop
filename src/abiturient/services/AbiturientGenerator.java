package abiturient.services;

import abiturient.abiturient.Abiturient;

public class AbiturientGenerator {
    // Получаем случайный массив абитуриентов
    public Abiturient[] getRandomAbiturientList(int numAbiturients, int numMarks, int minMark) {
        Abiturient[] list = new Abiturient[numAbiturients];
        for (int i = 0; i < numAbiturients; i++) {
            int[] marks = getRandomMarks(numMarks, minMark);
            list[i] = new Abiturient("abiturient" + (i + 1), marks);
        }
        return list;
    }

    // Получить случайный массив оценок размером numMarks,
    // и минимальной возможной оценкой в массиве minMark
    public int[] getRandomMarks(int numMarks, int minMark) {
        int[] marks = new int[numMarks];
        for (int i = 0; i < numMarks; i++) {
            marks[i] = (int) (Math.random() * (6 - minMark)) + minMark;
        }
        return marks;
    }
}
