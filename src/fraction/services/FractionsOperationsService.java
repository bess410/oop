package fraction.services;

import fraction.fraction.CommonFraction;

public class FractionsOperationsService {

    public CommonFraction reduce(CommonFraction fraction) {
        int numerator = fraction.getNumerator();
        int denominator = fraction.getDenominator();
        // Получаем наибольший общий делитель для числителя и знаменателя
        // и сокращаем на него нашу дробь
        int gcd = gcd(numerator, denominator);
        return new CommonFraction(numerator / gcd, denominator / gcd);
    }

    public CommonFraction subtract(CommonFraction minuend, CommonFraction subtrahend) {
        // Получаем общий знаменатель, как наименьшее общее кратное
        int denominator = lcm(minuend.getDenominator(), subtrahend.getDenominator());
        int numerator = minuend.getNumerator() * denominator / minuend.getDenominator() -
                subtrahend.getNumerator() * denominator / subtrahend.getDenominator();
        return reduce(new CommonFraction(numerator, denominator));
    }

    public CommonFraction add(CommonFraction addendOne, CommonFraction addendTwo) {
        // Получаем общий знаменатель, как наименьшее общее кратное
        int denominator = lcm(addendOne.getDenominator(), addendTwo.getDenominator());
        int numerator = addendOne.getNumerator() * denominator / addendOne.getDenominator() +
                addendTwo.getNumerator() * denominator / addendTwo.getDenominator();
        return reduce(new CommonFraction(numerator, denominator));
    }

    public CommonFraction divide(CommonFraction dividend, CommonFraction divisor) {
        int numerator = dividend.getNumerator() * divisor.getDenominator();
        if (divisor.getNumerator() == 0) {
            throw new IllegalStateException("The divisor is zero!");
        }
        int denominator = dividend.getDenominator() * divisor.getNumerator();
        return reduce(new CommonFraction(numerator, denominator));
    }

    public CommonFraction multiply(CommonFraction multiplicand, CommonFraction multiplier) {
        int numerator = multiplicand.getNumerator() * multiplier.getNumerator();
        int denominator = multiplicand.getDenominator() * multiplier.getDenominator();
        return reduce(new CommonFraction(numerator, denominator));
    }

    // Находит наибольший общий делитель
    private int gcd(int a, int b) {
        while (b != 0) {
            int tmp = a % b;
            a = b;
            b = tmp;
        }
        return a;
    }

    // Находим наименьшее общее кратное
    private int lcm(int a, int b) {
        return a * b / gcd(a, b);
    }
}
