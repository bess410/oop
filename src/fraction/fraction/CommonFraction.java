package fraction.fraction;

public class CommonFraction {
    private int numerator;
    private int denominator;

    public CommonFraction(int numerator, int denominator) {
        this.numerator = numerator;

        // Если в качестве знаменателя вводят 0,
        // то кидаем exception
        if (denominator == 0) {
            throw new IllegalStateException("The denominator is zero!");
        } else {
            this.denominator = denominator;
        }
    }

    public int getNumerator() {
        return numerator;
    }

    public int getDenominator() {
        return denominator;
    }

    public void setNumerator(int numerator) {
        this.numerator = numerator;
    }

    public void setDenominator(int denominator) {
        // Еще одна проверка на 0
        if (denominator == 0) {
            throw new IllegalStateException("The denominator is zero!");
        } else {
            this.denominator = denominator;
        }
    }

    @Override
    public String toString() {
        return numerator + "/" + denominator;
    }
}
