package fraction;

import fraction.fraction.CommonFraction;
import fraction.services.FractionsOperationsService;

public class TestFractionsOperations {
    public static void main(String[] args) {
        CommonFraction commonFractionOne = new CommonFraction(4, 8);
        CommonFraction commonFractionTwo = new CommonFraction(4, 16);
        FractionsOperationsService fos = new FractionsOperationsService();
        System.out.println("First fraction is " + commonFractionOne);
        System.out.println("Second fraction is " + commonFractionTwo);
        System.out.println("reduce " + commonFractionOne + " to " + fos.reduce(commonFractionOne));
        System.out.println("reduce " + commonFractionTwo + " to " + fos.reduce(commonFractionTwo));
        System.out.println();
        System.out.println(commonFractionOne + " add " + commonFractionTwo +
                "\nequals " + fos.add(commonFractionOne, commonFractionTwo));
        System.out.println();
        System.out.println(commonFractionOne + " subtract " + commonFractionTwo +
                "\nequals " + fos.subtract(commonFractionOne, commonFractionTwo));
        System.out.println();
        System.out.println(commonFractionOne + " multiply " + commonFractionTwo +
                "\nequals " + fos.multiply(commonFractionOne, commonFractionTwo));
        System.out.println();
        System.out.println(commonFractionOne + " divide " + commonFractionTwo +
                "\nequals " + fos.divide(commonFractionOne, commonFractionTwo));

    }
}
